import axios from 'axios';
import { Formik } from 'formik';
import React, { useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import * as Yup from 'yup';
import { GlobalContext } from '../../context/GlobalContext';
import Navbar from '../loading/Navbar';


const validationSchema = Yup.object().shape({
	nama: Yup.string().required('Nama wajib diisi'),
	harga: Yup.number()
		.min(1000, 'Minimal harga adalah 1000')
		.typeError('Harap masukkan angka yang valid')
		.required('Harga wajib diisi'),
	stock: Yup.number()
		.min(1, 'Minimal stock adalah 1')
		.typeError('Harap masukkan stock yang valid')
		.required('stock wajib diisi'),
	is_diskon: Yup.boolean(),
	harga_diskon: Yup.number().when('is_diskon', {
		is: 1,
		then: Yup.number()
			.typeError('Harap masukkan angka yang valid')
			.min(1000, 'Minimal harga adalah 1000')
			.required('Harap Masukan harga diskon'),
	}),
	image_url: Yup.string().url().required('Gambar wajib diisi'),
});

const initialState = {
	nama: '',
	harga: 0,
	harga_diskon: 0,
	is_diskon: false,
	image_url: '',
	stock: 0,
};

function Form() {
	const navigate = useNavigate();
	const { setLoading } = useContext(GlobalContext);

	const onSubmit = async values => {
		setLoading(true);
		try {
			const response = await axios.post('https://arhandev.xyz/public/api/quiz/products', values, {
				headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
			});
			navigate('/table');
		} catch (e) {
			console.log(e);
			alert(e.response.data.info);
		} finally {
			setLoading(false);
		}
	};
	return (
		<div>
			<Navbar />
			<div className="max-w-lg p-5 my-20 mx-auto border-2 border-black rounded-xl">
				<h1 className="text-center m-0 mb-8 text-3xl font-bold">Membuat Product</h1>
				<Formik initialValues={initialState} onSubmit={onSubmit} validationSchema={validationSchema}>
					{({ values, handleSubmit, handleBlur, handleChange, setFieldValue, errors, touched, dirty, isValid }) => (
						<form onSubmit={handleSubmit} className="flex flex-col gap-5">
							<div className="w-full grid grid-cols-3">
								<label>Nama:</label>
								<input
									placeholder="Masukan nama barang"
									className="col-span-2 border rounded-lg border-gray-400 py-0.5 px-2"
									onChange={handleChange}
									value={values.nama}
									onBlur={handleBlur}
									type="text"
									name="nama"
								/>
								<div></div>
								<div className="col-span-2 text-red-400 mb-3 text-base">{touched.nama && errors.nama}</div>
							</div>
							<div className="w-full grid grid-cols-3">
								<label>Harga:</label>
								<input
									placeholder="Masukan harga barang"
									className="col-span-2 border rounded-lg border-gray-400 py-0.5 px-2"
									onChange={handleChange}
									value={values.harga}
									onBlur={handleBlur}
									type="text"
									name="harga"
								/>
								<div></div>
								<div className="col-span-2 text-red-400 mb-3 text-base">{touched.harga && errors.harga}</div>
							</div>
							<div className="w-full grid grid-cols-3">
								<label>Aktikan Diskon </label>
								<div className="col-span-2">
									<input
										type="checkbox"
										onChange={e => {
											if (e.target.checked) {
												setFieldValue('is_diskon', 1);
											} else {
												setFieldValue('is_diskon', 0);
											}
										}}
										name="is_diskon"
										id=""
										checked={values.is_diskon}
										onBlur={handleBlur}
									/>
								</div>
								<div></div>
								<div className="col-span-2 text-red-400 mb-3 text-base">{touched.is_diskon && errors.is_diskon}</div>
							</div>
							{values.is_diskon === 1 && (
								<div className="w-full grid grid-cols-3">
									<label>Harga Diskonn:</label>
									<input
										placeholder="Masukan harga diskon barang"
										className="col-span-2 border rounded-lg border-gray-400 py-0.5 px-2"
										onChange={handleChange}
										value={values.harga_diskon}
										onBlur={handleBlur}
										type="text"
										name="harga_diskon"
									/>
									<div></div>
									<div className="col-span-2 text-red-400 mb-3 text-base">
										{touched.harga_diskon && errors.harga_diskon}
									</div>
								</div>
							)}
							<div className="w-full grid grid-cols-3">
								<label>Stock:</label>
								<input
									placeholder="Masukan stock barang"
									className="col-span-2 border rounded-lg border-gray-400 py-0.5 px-2"
									onChange={handleChange}
									value={values.stock}
									onBlur={handleBlur}
									type="text"
									name="stock"
								/>
								<div></div>
								<div className="col-span-2 text-red-400 mb-3 text-base">{touched.stock && errors.stock}</div>
							</div>
							<div className="w-full grid grid-cols-3">
								<label>Image Url:</label>
								<input
									placeholder="Masukan link gambar barang"
									className="col-span-2 border rounded-lg border-gray-400 py-0.5 px-2"
									onChange={handleChange}
									value={values.image_url}
									onBlur={handleBlur}
									type="text"
									name="image_url"
								/>
								<div></div>
								<div className="col-span-2 text-red-400 mb-3 text-base">{touched.image_url && errors.image_url}</div>
							</div>
							<div className="flex justify-center">
								<button
									className="bg-blue-300 text-black border-2 border-black font-bold rounded-xl text-xl py-2 px-5"
									disabled={!dirty && !isValid}
								>
									Create
								</button>
							</div>
						</form>
					)}
				</Formik>
			</div>
		</div>
	);
}

export default Form;
