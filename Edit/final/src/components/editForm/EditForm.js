import axios from 'axios';
import { Formik } from 'formik';
import React, { useContext, useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import * as Yup from 'yup';
import { GlobalContext } from '../../context/GlobalContext';
import Navbar from '../loading/Navbar';

const validationSchema = Yup.object().shape({
	nama: Yup.string().required('Nama wajib diisi'),
	harga: Yup.number()
		.typeError('Harap masukkan angka yang valid')
		.min(1000, 'Minimal harga adalah 1000')
		.required('Harga wajib diisi'),
	stock: Yup.number()
		.typeError('Harap masukkan angka yang valid')
		.min(1, 'Minimal stock adalah 1')
		.required('stock wajib diisi'),
	is_diskon: Yup.boolean(),
	harga_diskon: Yup.number().when('is_diskon', {
		is: 1,
		then: Yup.number()
			.typeError('Harap masukkan angka yang valid')
			.min(1000, 'Minimal harga adalah 1000')
			.required('Harap Masukan harga diskon'),
	}),
	image_url: Yup.string().url().required('Gambar wajib diisi'),
});

function EditForm() {
	const { id } = useParams();
	const navigate = useNavigate();
	const { setLoading } = useContext(GlobalContext);

	const [input, setInput] = useState({
		nama: '',
		harga: 0,
		harga_diskon: 0,
		is_diskon: false,
		image_url: '',
		stock: 0,
	});

	const handleChangeCheckbox = e => {
		if (e.target.checked) {
			setInput({ ...input, is_diskon: 1 });
		} else {
			setInput({ ...input, is_diskon: 0 });
		}
	};

	const handleChangeInput = e => {
		setInput({ ...input, [e.target.name]: e.target.value });
	};

	const onUpdate = async values => {
		setLoading(true);
		try {
			const response = await axios.put(`https://arhandev.xyz/public/api/products/${id}`, values);
			navigate('/table');
		} catch (e) {
			console.log(e);
			alert(e.response.data.info);
		} finally {
			setLoading(false);
		}
	};

	const fetchProduct = async () => {
		setLoading(true);
		try {
			const response = await axios.get(`https://arhandev.xyz/public/api/products/${id}`);
			setInput({
				nama: response.data.data.nama,
				harga: response.data.data.harga,
				harga_diskon: response.data.data.harga_diskon,
				is_diskon: response.data.data.is_diskon,
				image_url: response.data.data.image_url,
				stock: response.data.data.stock,
			});
		} catch (e) {
			console.log(e);
			alert(e.response.data.info);
		} finally {
			setLoading(false);
		}
	};

	useEffect(() => {
		fetchProduct();
	}, []);

	return (
		<div>
			<Navbar />

			<div className="max-w-lg p-5 my-20 mx-auto border-2 border-black rounded-xl">
				<h1 className="text-center m-0 mb-8 text-3xl font-bold">Mengupdate Product</h1>
				<Formik initialValues={input} onSubmit={onUpdate} validationSchema={validationSchema} enableReinitialize>
					{({ values, handleSubmit, handleBlur, handleChange, setFieldValue, errors, touched, dirty, isValid }) => (
						<form className="flex flex-col gap-5" onSubmit={handleSubmit}>
							<div className="w-full grid grid-cols-3">
								<label>Nama: </label>
								<input
									className="col-span-2 border rounded-lg border-gray-400 py-0.5 px-2"
									type="text"
									name="nama"
									placeholder="Masukan nama barang"
									value={values.nama}
									onChange={handleChange}
									onBlur={handleBlur}
								/>
								<div></div>
								<div className="col-span-2 text-red-400 mb-3 text-base">{touched.nama && errors.nama}</div>
							</div>
							<div className="w-full grid grid-cols-3">
								<label>Harga: </label>
								<input
									className="col-span-2 border rounded-lg border-gray-400 py-0.5 px-2"
									type="number"
									name="harga"
									value={values.harga}
									onChange={handleChange}
									onBlur={handleBlur}
								/>
								<div></div>
								<div className="col-span-2 text-red-400 mb-3 text-base">{touched.harga && errors.harga}</div>
							</div>
							<div className="w-full grid grid-cols-3">
								<label>Aktikan Diskon </label>
								<div className="checkbox">
									<input
										className="col-span-2 border rounded-lg border-gray-400 py-0.5 px-2"
										type="checkbox"
										onChange={e => {
											if (e.target.checked) {
												setFieldValue('is_diskon', 1);
											} else {
												setFieldValue('is_diskon', 0);
											}
										}}
										name="is_diskon"
										id=""
										checked={values.is_diskon}
										onBlur={handleBlur}
									/>
								</div>
								<div></div>
								<div className="col-span-2 text-red-400 mb-3 text-base">{touched.is_diskon && errors.is_diskon}</div>
							</div>
							{values.is_diskon === 1 && (
								<div className="w-full grid grid-cols-3">
									<label>Harga Diskon: </label>
									<input
										className="col-span-2 border rounded-lg border-gray-400 py-0.5 px-2"
										type="number"
										name="harga_diskon"
										value={values.harga_diskon}
										onChange={handleChange}
										onBlur={handleBlur}
									/>
									<div></div>
									<div className="col-span-2 text-red-400 mb-3 text-base">
										{touched.harga_diskon && errors.harga_diskon}
									</div>
								</div>
							)}
							<div className="w-full grid grid-cols-3">
								<label>Stock: </label>
								<input
									className="col-span-2 border rounded-lg border-gray-400 py-0.5 px-2"
									type="number"
									name="stock"
									value={values.stock}
									onChange={handleChange}
									onBlur={handleBlur}
								/>
								<div></div>
								<div className="col-span-2 text-red-400 mb-3 text-base">{touched.stock && errors.stock}</div>
							</div>
							<div className="w-full grid grid-cols-3">
								<label>Image: </label>
								<input
									className="col-span-2 border rounded-lg border-gray-400 py-0.5 px-2"
									type="text"
									name="image_url"
									placeholder="Masukan link gambar barang"
									value={values.image_url}
									onChange={handleChange}
									onBlur={handleBlur}
								/>
								<div></div>
								<div className="col-span-2 text-red-400 mb-3 text-base">{touched.image_url && errors.image_url}</div>
							</div>
							<div className="flex justify-center">
								<button
									className="bg-yellow-200 text-black border-2 border-black font-bold rounded-xl text-xl py-2 px-5"
									disabled={!isValid}
								>
									Save
								</button>
							</div>
						</form>
					)}
				</Formik>
			</div>
		</div>
	);
}

export default EditForm;
