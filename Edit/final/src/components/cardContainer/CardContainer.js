import axios from 'axios';
import React, { useContext, useEffect } from 'react';
import { GlobalContext } from '../../context/GlobalContext';
import Card from '../card/Card';
import Navbar from '../loading/Navbar';
import { Slide } from "react-slideshow-image";
import styles from './cardContainer.css';
import slideImages from "../../img/img";


function CardContainer() {
	const { products, setProducts, setLoading } = useContext(GlobalContext);

	const fetchProducts = async () => {
		try {
			setLoading(true);
			const response = await axios.get('https://arhandev.xyz/public/api/quiz/products');
			setProducts(response.data.data);
			setLoading(false);
		} catch (e) {
			console.log(e);
			alert('terjadi suatu error');
			//cara terbaik untuk setLoading false adalah memasukkannya ke dalam block finally
			// pelajarin lebih lanjut mengenai block finally
			setLoading(false);
		}
	};
	useEffect(() => {
		fetchProducts();
	}, []);

	return (
	<div>	
		
		<div className={styles.container}>
      <Slide easing="ease">
        {slideImages.map((slide, index) => {
          return (
            <div className={styles.slide} key={slide}>
              <div style={{ backgroundImage: `url(${slideImages[index]})` }}>
                <span>Slide {index + 1}</span>
              </div>
            </div>
          );
        })}
      </Slide>
    </div>
   

		<div>
			<Navbar />
			<div className="w-10/12 mx-auto">
				<h1 className="text-3xl mt-12 mb-8 font-bold">Catalog Product</h1>
				<div className="grid grid-cols-4 gap-10">
					{products.length === 0 ? (
						<h3>Tidak ada product</h3>
					) : (
						products.map((item, index) => <Card key={index} data={item} />)
					)}
				</div>
			</div>

		</div>
		</div>
	);
}

export default CardContainer;
