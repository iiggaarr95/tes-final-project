import { useContext } from 'react';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import './App.css';
import CardContainer from './components/cardContainer/CardContainer';
import EditForm from './components/editForm/EditForm';
import Form from './components/form/Form';
import Loading from './components/loading/Loading';
import Login from './components/login/Login';
import ProductTable from './components/productTable/ProductTable';
import Register from './components/register/Register';
import { GlobalContext } from './context/GlobalContext';
import ProtectedRoute from './inherit/ProtectedRoute';
import Slide from './components/slide/slide.js';
import Slideshow from './components/slide/slide.js';

const router = createBrowserRouter([
	{
		path: '/',
		element: <CardContainer />,
	},
	
	{
		path: '/login',
		element: <Login />,
	},
	

	{
		path: '/register',
		element: <Register />,
	},
	{
		element: <ProtectedRoute />,
		children: [
			{
				path: '/table',
				element: <ProductTable />,
			},
			{
				path: '/create',
				element: <Form />,
			},
			{
				path: '/edit/:id',
				element: <EditForm />,
			},
		],
	},
	
]);

function App() {
	const { loading } = useContext(GlobalContext);
	return (
		<div className="App">
			{loading && <Loading />}
			<RouterProvider router={router} />
		</div>
	);
}

export default App;
