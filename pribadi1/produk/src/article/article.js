import React from 'react';
import './article.css';

function Article(props) {
	return (
		<div className="article">
			<div className="image-container">
				<img src={props.article.image_url} alt="" />
			</div>
			<div className="content-container">
				<h1>Nama Produk : {props.article.nama}</h1>
				<p>Harga : Rp. {props.article.harga}</p>
                <p>Harga Diskon : Rp. {props.article.harga_diskon}</p>
                <p>Stock : {props.article.stock}</p>
			</div>
		</div>
	);
}

export default Article;
