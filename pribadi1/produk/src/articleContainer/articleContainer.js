import React from 'react';
import Article from '../article/article';
import './articleContainer.css';

function articleContainer(props) {
	return (
		<div className="article-container">
			{props.articles.map(article => (
				<Article article={article} />
			))}
		</div>
	);
}

export default articleContainer;
