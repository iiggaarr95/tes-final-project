import axios from 'axios';
import React, { useState } from 'react'; 
import './App.css';
import {useRef} from 'react';

function CreateForm ({ fetchUsers }) {
 	const [input, setInput] = useState({
    nama:'',
    harga:'',
    harga_diskon:'',
    is_diskon:'',
    stock:'',
    image_url:'',
    

  });

const text1 = useRef()
const check1 = useRef()


const handleType = event => {
  if (event.target.name === 'nama') {
    setInput({ ...input, nama:event.target.value});
    
  } else if (event.target.name === 'harga') {
    setInput({ ...input, harga:event.target.value});
    
  } else if (event.target.name === 'harga_diskon') { 
    setInput({ ...input, harga_diskon:event.target.value});
    
  } else if (event.target.name === 'is_diskon') { 
    console.log(event.target.checked)
    let is_diskon
    if(event.target.checked)
    {
      is_diskon = 1
    }
    else{
      is_diskon = 0
    }
    setInput({ ...input, is_diskon:is_diskon});
    
  } else if (event.target.name === 'stock') { 
    setInput({ ...input, stock:event.target.value});
    
  } else if (event.target.name === 'image_url') { 
    setInput({ ...input, image_url:event.target.value});
    
  } 
  text1.current.style.display = check1.current.checked ? 'block' : 'none'
     
} ;


  const submitHandler = async () => {
    try {
      const response = await axios.post('http://arhandev.xyz/public/api/products', {
        nama: input.nama,
        harga: input.harga,
        harga_diskon: input.harga_diskon,
        is_diskon: input.is_diskon,
        stock: input.stock,
        image_url: input.image_url,

      });
      fetchUsers(response.data);
      setInput (
        {
          nama:'',
          harga:'',
          harga_diskon:'',
          is_diskon:'',         
          stock:'',
          image_url:'',
        }
      );

    } catch (error) {
      alert(error.response.data.info);
    }
    
  };
  

  return (
    <div className='kotak-border'> 
    <div className='createForm'>
    <h1> Membuat Produk</h1>
     <br />
     <div style={{marginLeft : '10px'}}>
     <label> Nama : </label>
     <input onChange={handleType} type='string' placeholder='nama' name='nama'/>
      </div>
      <br />
     <br />
     <div style={{marginLeft : '10px'}}> 
     <label> Harga : </label>
     <input onChange={handleType} type='integer' placeholder='harga' name='harga' />
     </div>     
     <br />
     <br />
     <div style={{marginRight : '215px'}}> 
     <label>Aktifkan Diskon : 
     <input onChange={handleType} type='checkbox'  placeholder='is_diskon' name='is_diskon'   ref={check1}  />
     </label>
     
     
     <div style={{marginLeft : '125px'}}> 
          <input onChange={handleType} type="text" name="harga_diskon" style={{display: 'none'}} ref={text1} placeholder='harga_diskon' /> 
     
     </div>
     
     </div>
     
     <br />
     <br />
     <div style={{marginLeft : '10px'}}>
     <label> Stok : </label>
     <input onChange={handleType} type='integer' placeholder='stock' name='stock' />
     </div>
     
     <br />
     <br />
     <div style={{marginLeft : '0px'}}> 
     <label> Image : </label>
     <input onChange={handleType} type='url' placeholder='image_url' name='image_url' />
     </div>
    
     <div style= {{marginTop : '20px'}}> 
     <button onClick={submitHandler} className='btn-myButton'>
      Submit
     </button>
     </div>
     </div>
    </div>
    
 

  );
};

export default CreateForm;