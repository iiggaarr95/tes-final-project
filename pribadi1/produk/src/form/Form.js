import axios from 'axios';
import React, { useState } from 'react'; 
import './form.css'
import {useRef} from 'react';

function Form ({ props }) {
 	const [input, setInput] = useState({
    nama:'',
    harga:'',
    harga_diskon:'',
    is_diskon:'',
    stock:'',
    image_url:'',
    

  });

const text1 = useRef()
const check1 = useRef()


const handleType = event => {
  if (event.target.name === 'nama') {
    setInput({ ...input, nama:event.target.value});
    
  } else if (event.target.name === 'harga') {
    setInput({ ...input, harga:event.target.value});
    
  } else if (event.target.name === 'harga_diskon') { 
    setInput({ ...input, harga_diskon:event.target.value});
    
  } else if (event.target.name === 'is_diskon') { 
    console.log(event.target.checked)
    let is_diskon
    if(event.target.checked)
    {
      is_diskon = 1
    }
    else{
      is_diskon = 0
    }
    setInput({ ...input, is_diskon:is_diskon});
    
  } else if (event.target.name === 'stock') { 
    setInput({ ...input, stock:event.target.value});
    
  } else if (event.target.name === 'image_url') { 
    setInput({ ...input, image_url:event.target.value});
    
  } 
  text1.current.style.display = check1.current.checked ? 'block' : 'none'
     
} ;


  const submitHandler = async () => {
    try {
      const response = await axios.post('http://arhandev.xyz/public/api/products', {
        nama: input.nama,
        harga: input.harga,
        harga_diskon: input.harga_diskon,
        is_diskon: input.is_diskon,
        stock: input.stock,
        image_url: input.image_url,

      });

      props.fetchArticles(response.data);
      setInput (
        {
          nama:'',
          harga:'',
          harga_diskon:'',
          is_diskon:'',         
          stock:'',
          image_url:'',
        }
      );

    } catch (error) {
      alert(error.response.data.info);
    }
    
  };
  

  return ( 
    <div className='Form'>
    <h1> List Catalog</h1>
    
     </div>
    
    
 

  );
};

export default Form;