import axios from 'axios';
import React, { useEffect, useState } from 'react';
import './App.css';
import {useRef} from 'react';

function EditForm ({ fetchUsers, edit, setEdit, }) {
  
  const [input, setInput] = useState({
   nama:'',
   harga:'',
   harga_diskon:'',
   is_diskon:'',
   stock:'',
   image_url:'',
   
   

 });

const text1 = useRef()
const check1 = useRef()


const handleType = event => {
  if (event.target.name === 'nama') {
    setInput({ ...input, nama:event.target.value});
    
  } else if (event.target.name === 'harga') {
    setInput({ ...input, harga:event.target.value});
    
  } else if (event.target.name === 'harga_diskon') { 
    setInput({ ...input, harga_diskon:event.target.value});
    
  } else if (event.target.name === 'is_diskon') { 
    console.log(event.target.checked)
    let is_diskon
    if(event.target.checked)
    {
      is_diskon = 1
    }
    else{
      is_diskon = 0
    }
    setInput({ ...input, is_diskon:is_diskon});
    
  } else if (event.target.name === 'stock') { 
    setInput({ ...input, stock:event.target.value});
    
  } else if (event.target.name === 'image_url') { 
    setInput({ ...input, image_url:event.target.value});
    
  } 
  text1.current.style.display = check1.current.checked ? 'block' : 'none'
     
} ;


 const submitHandler = async () => {
   try {
     const response = await axios.put(`http://arhandev.xyz/public/api/products/${edit.id}`, {
       nama: input.nama,
       harga: input.harga,
       harga_diskon: input.harga_diskon,
       is_diskon: input.is_diskon,
       stock: input.stock,
       image_url: input.image_url,
       

     });
     fetchUsers(response.data);
     setInput (
       {
        nama:'',
        harga:'',
        harga_diskon:'',
        is_diskon:'',
        stock:'',
        image_url:'',
       }
     ); setEdit({})

   } catch (error) {
     alert(error.response.data.info);
   }  
 };

 useEffect(() => {
    setInput({
        nama: edit.nama,
        harga: edit.harga,
        harga_diskon: edit.harga_diskon,
        is_diskon: edit.is_diskon,
        stock: edit.stock,
        image_url: edit.image_url,
    });
}, []);
 

 return (
   <div className='kotak-border'> 
   <div className='editForm'>
   <h1> Membuat Produk</h1>
     <br />
     <div style={{marginLeft : '10px'}}>
     <label> Nama : </label>
     <input onChange={handleType} type='string' placeholder='nama' name='nama' value={input.nama}/>
      </div>
      <br />
     <br />
     <div style={{marginLeft : '10px'}}> 
     <label> Harga : </label>
     <input onChange={handleType} type='integer' placeholder='harga' name='harga' value={input.harga} />
     </div>     
     <br />
     <br />
     <div style={{marginRight : '215px'}}> 
     <label>Aktifkan Diskon : 
     <input onChange={handleType} type='checkbox'  placeholder='is_diskon' name='is_diskon' value={input.is_diskon}  ref={check1}  />
     </label>
     
     
     <div style={{marginLeft : '125px'}}> 
          <input onChange={handleType} type="text" name="harga_diskon" value={input.harga_diskon} style={{display: 'none'}} ref={text1} placeholder='harga_diskon'  /> 
     
     </div>
     
     </div>
     
     <br />
     <br />
     <div style={{marginLeft : '10px'}}>
     <label> Stok : </label>
     <input onChange={handleType} type='integer' placeholder='stock' name='stock' value={input.stock} />
     </div>
     
     <br />
     <br />
     <div style={{marginLeft : '0px'}}> 
     <label> Image : </label>
     <input onChange={handleType} type='url' placeholder='image_url' name='image_url' value={input.image_url}/>
     </div>
    
     <div style= {{marginTop : '20px'}}> 
     <button onClick={submitHandler} className='btn-myButton' type='button'>
      Submit
     </button>
     </div>
    
     </div>
    </div>
    
 

  );
};


export default EditForm;