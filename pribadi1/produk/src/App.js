import axios from 'axios';
import { useEffect, useState } from 'react';
import './App.css';
import CreateForm from './createForm';
import EditForm from './editForm';
import ArticleContainer from './articleContainer/articleContainer';
import Form from './form/Form';
//import article from './article/article';


function App() {
	const [users, setUsers] = useState([]);
	const [edit, setEdit] = useState({});
	const [Articles, setArticles] = useState([]);

	const fetchUsers = async () => {
		try {
			const response = await axios.get('http://arhandev.xyz/public/api/products');
			setUsers(response.data.data);
		} catch (error) {
			console.log(error);
		}
	};

	const deleteUser = async userId => {
		try {
			const response = await axios.delete(`http://arhandev.xyz/public/api/products/${userId}`);
			fetchUsers(response.data);
		} catch (error) {
			console.log(error);
		}
	};

	useEffect(() => {
		fetchUsers();
	}, []);

	const fetchArticles = async () => {
		try {
			const response = await axios.get('http://arhandev.xyz/public/api/products');
			setArticles(response.data.data)
		} catch (error) {
			console.log(error);
		}
	};

	useEffect(() => {
		fetchArticles();
	}, []);

	return (
		<div className="App">
			
			{Object.keys(edit).length === 0 ? (
				<CreateForm fetchUsers={fetchUsers} />
			) : (
				<EditForm fetchUsers={fetchUsers} edit={edit} setEdit={setEdit} />
			)}

<h1> </h1>
			<table className='blueTable'>
				<thead>
					<tr>
						<th>Nama</th>
						<th>Harga</th>
						<th>Harga Diskon</th>
						<th>Diskon</th>
						<th>Stock</th>
						<th> Image URL</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					{users.map((user, index) => (
						<tr key={index}>
							<td>{user.nama}</td>
							<td>{user.harga}</td>
							<td>{user.harga_diskon}</td>
							<td>{user.is_diskon}</td>
							<td>{user.stock}</td>
							<td>{user.image_url}</td>
							<td>
								<div className="action">
									<button onClick={() => setEdit(user)} className="btn-edit">
										Edit
									</button>
									<button onClick={() => deleteUser(user.id)} className="btn-delete">
										Delete
									</button>
								</div>
							</td>
						</tr>
					))}
				</tbody>
			</table>

			<div className="App">
			<Form articles={Articles} setArticles={setArticles} fetchArticles={fetchArticles} />
			<ArticleContainer articles={Articles} />
		</div>	
		</div>

		
	);
}

export default App;
