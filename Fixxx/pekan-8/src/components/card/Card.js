import React from 'react';

function Card({ data }) {
	return (
		<div className="overflow-hidden rounded-2xl border-2 border-gray-400 shadow-md">
			<img className="w-full" src={data.image_url} alt="" />
			<div className="p-2">
				<p className="text-2xl font-bold">{data.nama}</p>
				{data.is_diskon === 1 ? (
					<div>
						<p className="text-blue-800 text-sm -mb-1 line-through">Rp {data.harga_display}</p>
						<p className="text-xl text-red-500 font-bold">Rp {data.harga_diskon_display}</p>
					</div>
				) : (
					<p className="text-xl text-blue-800 font-bold">Rp {data.harga_display}</p>
				)}
				<p className="text-lg text-blue-500">Stock {data.stock}</p>
			</div>
		</div>
	);
}

export default Card;
