import axios from 'axios';
import React from 'react';
import { Link, useNavigate } from 'react-router-dom';

function Navbar() {
	const navigate = useNavigate();

	const onLogout = async () => {
		try {
			const response = await axios.post(
				'https://arhandev.xyz/public/api/logout',
				{},
				{ headers: { Authorization: `Bearer ${localStorage.getItem('token')}` } }
			);
			localStorage.removeItem('token');
			localStorage.removeItem('username');
			navigate('/');
		} catch (error) {
			alert(error.response.data.info);
		}
	};
	return (
		<div className="bg-green-600 py-4 text-white text-xl shadow-md">
			<div className="w-10/12 mx-auto flex justify-between items-center">
				<nav className="flex gap-8">
					<Link to="/">
						<div>Home</div>
					</Link>
					<Link to="/table">
						<div>Table</div>
					</Link>
				</nav>
				{localStorage.getItem('token') === null ? (
					<nav className="flex gap-4">
						<Link to="/login">
							<div className="px-6 border-2 border-white bg-green-600 text-white rounded-lg font-bold py-1 ">Login</div>
						</Link>
						<Link to="/register">
							<div className="px-6 border-2 border-green-600 bg-white text-green-600 rounded-lg font-bold py-1 ">
								Register
							</div>
						</Link>
					</nav>
				) : (
					<nav className="flex gap-4 items-center">
						<div className="font-bold">Hi, {localStorage.getItem('username')}</div>
						<button onClick={onLogout} className="px-6 py-1 border-2 border-red-400 text-red-400 bg-white rounded-lg">
							Logout
						</button>
					</nav>
				)}
			</div>
		</div>
	);
}

export default Navbar;
