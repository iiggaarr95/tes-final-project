import React from 'react';
import Article from '../article/article';
import './articleContainer.css';
import { UseContext, UseEffect } from 'react';
import axios from 'axios';
import { GlobalContext } from '../context/GlobalContext';
import Navbar from '../loading/navbar';

function articleContainer() {

	const { articles, setLoading, setArticles } = UseContext(GlobalContext);
	const fetchArticles = async () => {
		setLoading(true);
		try {
			setLoading(true);
			const response = await axios.get('hhttp://arhandev.xyz/public/api/products');
			setArticles(response.data.data);
			setLoading(false);
		} catch (error) {
			console.log(error);
			alert('Error bos');
			setLoading(false);
		} 
		}

		UseEffect(() => {
			fetchArticles();
		}, []);
	
		return (
			<div>
				<Navbar />
				<div style={{ maxWidth: '80%', margin: 'auto' }}>
					<h1>Catalog Product</h1>
					<div className="article-container">
						{articles.length === 0 ? (
							<h3>Tidak ada product</h3>
						) : (
							articles.map((item, index) => <Article key={index} data={item} />)
						)}
					</div>
				</div>
			</div>
		);
	
	};

	


export default articleContainer;
