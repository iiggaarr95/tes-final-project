import { useContext } from 'react';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import './App.css';
import Login from './login/Login';
import EditForm from './editForm';
import ArticleContainer from './articleContainer/articleContainer';
import ProductTable from './productTable/ProductTable';
import Form from './form/Form';
import { GlobalContext } from './context/GlobalContext';
import Loading from './loading/loading';
import Register from './register/Register';
import ProtectedRoute from './inherit/ProtectedRoute';

const router = createBrowserRouter([
	{
		path: '/',
		element: <ArticleContainer />,
	},
	
	{
		path: '/login',
		element: <Login />,
	},
	

	{
		path: '/register',
		element: <Register />,
	},
	{
		element: <ProtectedRoute />,
		children: [
			{
				path: '/table',
				element: <ProductTable />,
			},
			{
				path: '/create',
				element: <Form />,
			},
			{
				path: '/edit/:id',
				element: <EditForm />,
			},
		],
	},
	
]);

function App() {
	const { loading } = useContext(GlobalContext);
	return (
		<div className="App">
			{loading && <Loading />}
			<RouterProvider router={router} />
		</div>
	);
}

export default App;
