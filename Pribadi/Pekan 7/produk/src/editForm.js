import axios from 'axios';
import { Formik } from 'formik';
import React, { useEffect, useState } from 'react';
import './App.css';
import {useRef} from 'react';
import * as Yup from 'yup';

const validationSchema = Yup.object().shape({
	nama: Yup.string().required('Nama wajib diisi'),
	harga: Yup.number()
		.typeError('Harap masukkan angka yang valid')
		.min(1000, 'Minimal harga adalah 1000')
		.required('Harga wajib diisi'),
	stock: Yup.number()
		.typeError('Harap masukkan angka yang valid')
		.min(1, 'Minimal stock adalah 1')
		.required('stock wajib diisi'),
	is_diskon: Yup.boolean(),
	harga_diskon: Yup.number().when('is_diskon', {
		is: 1,
		then: Yup.number()
			.typeError('Harap masukkan angka yang valid')
			.min(1000, 'Minimal harga adalah 1000')
			.required('Harap Masukan harga diskon'),
	}),
	image_url: Yup.string().url().required('Gambar wajib diisi'),
});


function EditForm () {
  const { id } = useParams();
	const navigate = useNavigate();
	const { setLoading } = useContext(GlobalContext);

  const [input, setInput] = useState({
   nama:'',
   harga:0,
   harga_diskon:0,
   is_diskon:false,
   stock:0,
   image_url:'',
   
   

 });

const text1 = useRef()
const check1 = useRef()


const handleType = event => {
  if (event.target.name === 'nama') {
    setInput({ ...input, nama:event.target.value});
    
  } else if (event.target.name === 'harga') {
    setInput({ ...input, harga:event.target.value});
    
  } else if (event.target.name === 'harga_diskon') { 
    setInput({ ...input, harga_diskon:event.target.value});
    
  } else if (event.target.name === 'is_diskon') { 
    console.log(event.target.checked)
    let is_diskon
    if(event.target.checked)
    {
      is_diskon = 1
    }
    else{
      is_diskon = 0
    }
    setInput({ ...input, is_diskon:is_diskon});
    
  } else if (event.target.name === 'stock') { 
    setInput({ ...input, stock:event.target.value});
    
  } else if (event.target.name === 'image_url') { 
    setInput({ ...input, image_url:event.target.value});
    
  } 
  text1.current.style.display = check1.current.checked ? 'block' : 'none'
     
} ;


 const submitHandler = async values => {
  setLoading(true);
  try {
    const response = await axios.put(`http://arhandev.xyz/public/api/products/${id}`, values);
    navigate('/table');
  } catch (e) {
    console.log(e);
    alert(e.response.data.info);
  } finally {
    setLoading(false);
  }
};

const fetchProduct = async () => {
  setLoading(true);
  try {
    const response = await axios.get(`http://arhandev.xyz/public/api/products/${id}`);
    setInput({
      nama: response.data.data.nama,
      harga: response.data.data.harga,
      harga_diskon: response.data.data.harga_diskon,
      is_diskon: response.data.data.is_diskon,
      image_url: response.data.data.image_url,
      stock: response.data.data.stock,
    });
  } catch (e) {
    console.log(e);
    alert(e.response.data.info);
  } finally {
    setLoading(false);
  }
};

useEffect(() => {
  fetchProduct();
}, []);

 return (
   <div className='kotak-border'> 
   <div className='editForm'>
   <h1 style={{ textAlign: 'center', margin: '0px', marginBottom: '20px' }}> Membuat Produk</h1>
   <Formik initialValues={input} onSubmit={onUpdate} validationSchema={validationSchema} enableReinitialize>

   
     <br />
     <div style={{marginLeft : '10px'}}>
     <label> Nama : </label>
     <input onChange={handleType} type='string' placeholder='nama' name='nama' value={input.nama}/>
      </div>
      <br />
     <br />
     <div style={{marginLeft : '10px'}}> 
     <label> Harga : </label>
     <input onChange={handleType} type='integer' placeholder='harga' name='harga' value={input.harga} />
     </div>     
     <br />
     <br />
     <div style={{marginRight : '215px'}}> 
     <label>Aktifkan Diskon : 
     <input onChange={handleType} type='checkbox'  placeholder='is_diskon' name='is_diskon' value={input.is_diskon}  ref={check1}  />
     </label>
     
     
     <div style={{marginLeft : '125px'}}> 
          <input onChange={handleType} type="text" name="harga_diskon" value={input.harga_diskon} style={{display: 'none'}} ref={text1} placeholder='harga_diskon'  /> 
     
     </div>
     
     </div>
     
     <br />
     <br />
     <div style={{marginLeft : '10px'}}>
     <label> Stok : </label>
     <input onChange={handleType} type='integer' placeholder='stock' name='stock' value={input.stock} />
     </div>
     
     <br />
     <br />
     <div style={{marginLeft : '0px'}}> 
     <label> Image : </label>
     <input onChange={handleType} type='url' placeholder='image_url' name='image_url' value={input.image_url}/>
     </div>
    
     <div style= {{marginTop : '20px'}}> 
     <button onClick={submitHandler} className='btn-myButton' type='button'>
      Submit
     </button>
     </div>
    
     </Formik>
     </div>
    </div>
    
 

  );
};


export default EditForm;